package vboled.netcracker.musicstreamer.shared.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import vboled.netcracker.musicstreamer.config.ApplicationConfiguration;
import vboled.netcracker.musicstreamer.util.JwtDecoder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    private final ObjectMapper objectMapper;
    private final JwtDecoder jwtDecoder;
    private final ApplicationConfiguration.SecurityConfiguration.JwtConfiguration jwtConfiguration;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  JwtDecoder jwtDecoder,
                                  ObjectMapper objectMapper,
                                  ApplicationConfiguration.SecurityConfiguration.JwtConfiguration jwtConfiguration) {
        super(authenticationManager);
        this.jwtDecoder = jwtDecoder;
        this.objectMapper = objectMapper;
        this.jwtConfiguration = jwtConfiguration;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException, ExpiredJwtException {
        UsernamePasswordAuthenticationToken authentication;
        try {
            authentication = getAuthentication(request);
        } catch (JwtException | UsernameNotFoundException e) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON.toString());
            response.getWriter().write(objectMapper.writeValueAsString(e.getMessage()));
            return;
        }
        if (authentication == null) {
            filterChain.doFilter(request, response);
            return;
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request)
            throws JwtException, UsernameNotFoundException {
        //String token = request.getHeader(jwtConfiguration.getHeader());
        if (request.getCookies() == null) {
            throw new JwtException("no cookies");
        }
        Cookie authCookie = Arrays
                .stream(request.getCookies())
                .filter(cookie -> cookie.getName().equals(jwtConfiguration.getHeader()))
                .findFirst()
                .orElseThrow(() -> new JwtException("no auth cookies"));
        return jwtDecoder.getAuthenticationFromToken(authCookie.getValue());
    }
}

package vboled.netcracker.musicstreamer.shared.mapper;

import org.mapstruct.Mapper;
import vboled.netcracker.musicstreamer.dto.UserDTO;
import vboled.netcracker.musicstreamer.model.user.User;

@Mapper(componentModel = "spring")
public interface UserMapper extends AbstractMapper<User, UserDTO> {
}

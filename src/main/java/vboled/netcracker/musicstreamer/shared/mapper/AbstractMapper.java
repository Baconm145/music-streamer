package vboled.netcracker.musicstreamer.shared.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Named;

import java.util.List;

public interface AbstractMapper<E, D> {
    @Named("toDTO")
    D toDTO(E entity);

    @IterableMapping(qualifiedByName = "toDTO")
    List<D> toDTOList(List<E> entityList);
}

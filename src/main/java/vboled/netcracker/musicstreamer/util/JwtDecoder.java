package vboled.netcracker.musicstreamer.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vboled.netcracker.musicstreamer.config.ApplicationConfiguration;
import vboled.netcracker.musicstreamer.dto.form.UserSearchForm;
import vboled.netcracker.musicstreamer.model.user.User;
import vboled.netcracker.musicstreamer.service.UserService;

import java.util.Collections;
import java.util.List;

@Component
public class JwtDecoder {
    private final UserService userService;
    private final ApplicationConfiguration.SecurityConfiguration.JwtConfiguration jwtConfiguration;

    @Autowired
    public JwtDecoder(UserService userService,
                      ApplicationConfiguration configuration) {
        this.userService = userService;
        this.jwtConfiguration = configuration.getSecurityConfiguration().getJwtConfiguration();
    }

    public UsernamePasswordAuthenticationToken getAuthenticationFromToken(String token) {
        if (StringUtils.hasLength(token)) {
            byte[] signingKey = jwtConfiguration.getSecret().getBytes();

            Jws<Claims> parsedToken = Jwts.parser()
                    .setSigningKey(signingKey)
                    .parseClaimsJws(token);

            String username = parsedToken
                    .getBody()
                    .getSubject();

            List<SimpleGrantedAuthority> authorities = Collections
                    .singletonList(new SimpleGrantedAuthority((String) parsedToken.getBody().get("role")));

            User user = userService.read(new UserSearchForm().setUserName(username));

            if (StringUtils.hasLength(username)) {
                return new UsernamePasswordAuthenticationToken(user, null, authorities);
            }
        }
        return null;
    }
}

package vboled.netcracker.musicstreamer.dto.form;

public class UserSearchForm {
    private String userName;
    private String phoneNumber;
    private String email;

    public String getUserName() {
        return userName;
    }

    public UserSearchForm setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public UserSearchForm setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserSearchForm setEmail(String email) {
        this.email = email;
        return this;
    }
}

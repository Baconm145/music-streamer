package vboled.netcracker.musicstreamer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vboled.netcracker.musicstreamer.config.ApplicationConfiguration;
import vboled.netcracker.musicstreamer.dto.AuthDTO;
import vboled.netcracker.musicstreamer.service.AuthService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthService authService;
    private final ApplicationConfiguration.SecurityConfiguration securityConfiguration;

    public AuthController(AuthService authService, ApplicationConfiguration configuration) {
        this.securityConfiguration = configuration.getSecurityConfiguration();
        this.authService = authService;
    }

    @GetMapping("/access-token")
    public AuthDTO getToken(@RequestParam String login, @RequestParam String password, HttpServletResponse response) {
        String cookieName = securityConfiguration.getJwtConfiguration().getHeader();
        Cookie cookie = new Cookie(
                cookieName,
                authService.createJwtToken(authService.validateCredentials(login, password))
                        .getAccessToken()
        );
        response.addCookie(cookie);
        return null;
    }
}

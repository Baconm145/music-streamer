package vboled.netcracker.musicstreamer.service;

import vboled.netcracker.musicstreamer.dto.form.UserSearchForm;
import vboled.netcracker.musicstreamer.model.user.User;

public interface UserService {
    User read(UserSearchForm form);
    User readByCredentials(String login);
}

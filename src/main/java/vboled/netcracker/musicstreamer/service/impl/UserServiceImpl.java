package vboled.netcracker.musicstreamer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import vboled.netcracker.musicstreamer.dto.form.UserSearchForm;
import vboled.netcracker.musicstreamer.model.user.User;
import vboled.netcracker.musicstreamer.repository.UserRepository;
import vboled.netcracker.musicstreamer.repository.specification.UserSpecification;
import vboled.netcracker.musicstreamer.service.UserService;

import javax.persistence.EntityManager;


@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private final String USER_PATTERN = "^[a-zA-z]+(\\w{0,30})$";

    private final String EMAIL_PATTERN =
            "^([A-Za-z\\d])([\\w-.]{0,20})([A-Za-z\\d])@([A-Za-z\\d]([A-Za-z\\d-])*[A-Za-z\\d].)*" +
            "([A-Za-z\\d]([A-Za-z\\d-])*[A-Za-z\\d])" +
            "(.[A-Za-z]+)$";

    private final String PHONE_PATTERN = "^(\\d{1,3})(\\d{10})$";

    private final UserRepository userRepository;

    private final EntityManager entityManager;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12);

    @Autowired
    public UserServiceImpl(UserRepository userRepository, EntityManager entityManager) {
        this.userRepository = userRepository;
        this.entityManager = entityManager;
    }



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findOne(UserSpecification.searchByUserName(username)).orElseThrow(RuntimeException::new);
    }

    @Override
    public User read(UserSearchForm form) {
        return userRepository.findOne(UserSpecification.searchByFormAnd(form)).orElseThrow(RuntimeException::new);
    }

    @Override
    public User readByCredentials(String login) {
        return userRepository.findOne(UserSpecification.searchByFormOr(
                new UserSearchForm()
                        .setEmail(login)
                        .setPhoneNumber(login)
                        .setUserName(login)
        )).orElseThrow(RuntimeException::new);
    }
}

package vboled.netcracker.musicstreamer.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import javax.annotation.PostConstruct;

@ConfigurationProperties(prefix = "music-streamer")
@ConstructorBinding
public class ApplicationConfiguration {
    private final Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);

    private final SecurityConfiguration securityConfiguration;

    public ApplicationConfiguration(SecurityConfiguration security) {
        this.securityConfiguration = security;
    }

    public SecurityConfiguration getSecurityConfiguration() {
        return securityConfiguration;
    }

    public static final class SecurityConfiguration {
        private final JwtConfiguration jwtConfiguration;

        public SecurityConfiguration(JwtConfiguration jwt) {
            this.jwtConfiguration = jwt;
        }

        public JwtConfiguration getJwtConfiguration() {
            return jwtConfiguration;
        }

        public static final class JwtConfiguration {
            private final String secret;
            private final String type;
            private final String issuer;
            private final String audience;
            private final String prefix;
            private final String header;
            private final Long expireTimeSeconds;

            public JwtConfiguration(String secret, String type, String issuer, String audience, String prefix, String header, Long expireTimeSeconds) {
                this.secret = secret;
                this.type = type;
                this.issuer = issuer;
                this.audience = audience;
                this.prefix = prefix;
                this.header = header;
                this.expireTimeSeconds = expireTimeSeconds;
            }

            public String getSecret() {
                return secret;
            }

            public String getType() {
                return type;
            }

            public String getIssuer() {
                return issuer;
            }

            public String getAudience() {
                return audience;
            }

            public Long getExpireTimeSeconds() {
                return expireTimeSeconds;
            }

            public String getPrefix() {
                return prefix;
            }

            public String getHeader() {
                return header;
            }
        }
    }

    @Override
    public String toString() {
        return "ApplicationConfiguration{" +
                "securityConfiguration=" + securityConfiguration +
                '}';
    }

    @PostConstruct
    public void postInit(){
        logger.info(this.toString());
    }
}

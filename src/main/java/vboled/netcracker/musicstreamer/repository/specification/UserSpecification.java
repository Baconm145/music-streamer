package vboled.netcracker.musicstreamer.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import vboled.netcracker.musicstreamer.dto.form.UserSearchForm;
import vboled.netcracker.musicstreamer.model.user.User;
import vboled.netcracker.musicstreamer.model.user.User_;

import java.util.ArrayList;
import java.util.List;

public class UserSpecification {
    public static Specification<User> searchByUserName(String userName) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(User_.USER_NAME), userName);
    }

    public static Specification<User> searchByEmail(String email) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(User_.EMAIL), email);
    }

    public static Specification<User> searchByPhoneNumber(String phoneNumber) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(User_.PHONE_NUMBER), phoneNumber);
    }

    public static List<Specification<User>> createFormSpecifications(UserSearchForm form) {
        List<Specification<User>> specifications = new ArrayList<>();

        if (form.getEmail() != null) {
            specifications.add(searchByEmail(form.getEmail()));
        }

        if (form.getPhoneNumber() != null) {
            specifications.add(searchByPhoneNumber(form.getEmail()));
        }

        if (form.getUserName() != null) {
            specifications.add(searchByUserName(form.getUserName()));
        }

        return specifications;
    }

    public static Specification<User> searchByFormAnd(UserSearchForm form) {
        return createFormSpecifications(form).stream().reduce(Specification::and).orElse(null);
    }

    public static Specification<User> searchByFormOr(UserSearchForm form) {
        return createFormSpecifications(form).stream().reduce(Specification::or).orElse(null);
    }
}

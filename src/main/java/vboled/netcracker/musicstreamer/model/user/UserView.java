package vboled.netcracker.musicstreamer.model.user;

public class UserView {

    public UserView(User user) {
        this.userName = user.getUsername();
        this.name = user.getName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.phoneNumber = user.getPhoneNumber();
        this.regionName = "Implement_this";
    }

    private String userName;

    private String name;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String regionName;

    public String getUserName() {
        return userName;
    }

    public UserView setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserView setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserView setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserView setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public UserView setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getRegionName() {
        return regionName;
    }

    public UserView setRegionName(String regionName) {
        this.regionName = regionName;
        return this;
    }
}
